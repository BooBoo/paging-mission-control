package org.enlighten.challenge.mission;

import org.junit.Test;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import static org.enlighten.challenge.mission.Common.MINUTES;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class TelemetryMonitorTest
{
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
    
    @Test
    public void testAnalyzeFullDataset() throws Exception
    {
        List<SatelliteAlert> expectedAlerts = new LinkedList<>();
        expectedAlerts.add(new SatelliteAlert(1000, "RED HIGH", "TSTAT", DATE_FORMAT.parse("20180101 23:01:38.001")));
        expectedAlerts.add(new SatelliteAlert(1000, "RED LOW", "BATT", DATE_FORMAT.parse("20180101 23:01:09.521")));

        StringReader dataset = new StringReader("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n" +
                "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT\n" +
                "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT\n" +
                "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT\n" +
                "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT\n" +
                "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT\n" +
                "20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT\n" +
                "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT\n" +
                "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT\n" +
                "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT\n" +
                "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT\n" +
                "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT\n" +
                "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT\n" +
                "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT\n");
        
        TelemetryRecordReader in = new TelemetryRecordReader(dataset);
        List<AlertMonitor> alertMonitors = new LinkedList<>();
        alertMonitors.add(new AlertMonitor("TSTAT", "RED HIGH", 3, 5*MINUTES, telemetryRecord -> telemetryRecord.redHighLimitExceeded() && "TSTAT".equals(telemetryRecord.getComponent())));
        alertMonitors.add(new AlertMonitor("BATT", "RED LOW", 3, 5*MINUTES, telemetryRecord -> telemetryRecord.redLowLimitExceeded() && "BATT".equals(telemetryRecord.getComponent())));
        
        TelemetryMonitor telemetryMonitor = new TelemetryMonitor(alertMonitors);
        List<SatelliteAlert> alerts = telemetryMonitor.analyzeTelemetry(in);
        assertThat(alerts, is(equalTo(expectedAlerts)));
    }
}
