package org.enlighten.challenge.mission;

import org.junit.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.enlighten.challenge.mission.Common.MINUTES;
import static org.enlighten.challenge.mission.TestCommon.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SatelliteMonitorTest
{
    @Test
    public void testLogTelemetry() throws Exception
    {
        List<AlertMonitor> alertMonitors = new LinkedList<>();
        alertMonitors.add(new AlertMonitor("TSTAT", "RED HIGH", 3, 5*MINUTES, telemetryRecord -> telemetryRecord.redHighLimitExceeded() && "TSTAT".equals(telemetryRecord.getComponent())));
        alertMonitors.add(new AlertMonitor("BATT", "RED LOW", 3, 5*MINUTES, telemetryRecord -> telemetryRecord.redLowLimitExceeded() && "BATT".equals(telemetryRecord.getComponent())));
        SatelliteMonitor monitor = new SatelliteMonitor("1000", alertMonitors);
        
        List<SatelliteAlert> expectedHighAlert = new LinkedList<>();
        expectedHighAlert.add(new SatelliteAlert(1000, "RED HIGH", "TSTAT", TEST_DATE_FORMAT.parse("21:12:00")));
    
        assertThat(monitor.logTelemetry(createRedHighTelemetryRecord("21:12:00")), is(equalTo(Collections.emptyList())));
        assertThat(monitor.logTelemetry(createRedHighTelemetryRecord("21:13:00")), is(equalTo(Collections.emptyList())));
        assertThat(monitor.logTelemetry(createNeutralTelemetryRecord("21:13:12")), is(equalTo(Collections.emptyList())));
        assertThat(monitor.logTelemetry(createRedHighTelemetryRecord("21:14:00")), is(equalTo(expectedHighAlert)));
    
        List<SatelliteAlert> expectedLowAlert = new LinkedList<>();
        expectedLowAlert.add(new SatelliteAlert(1000, "RED LOW", "BATT", TEST_DATE_FORMAT.parse("21:12:00")));
        
        assertThat(monitor.logTelemetry(createRedLowTelemetryRecord("21:12:00")), is(equalTo(Collections.emptyList())));
        assertThat(monitor.logTelemetry(createRedLowTelemetryRecord("21:13:00")), is(equalTo(Collections.emptyList())));
        assertThat(monitor.logTelemetry(createNeutralTelemetryRecord("21:13:12")), is(equalTo(Collections.emptyList())));
        assertThat(monitor.logTelemetry(createRedLowTelemetryRecord("21:14:00")), is(equalTo(expectedLowAlert)));
    }
}
