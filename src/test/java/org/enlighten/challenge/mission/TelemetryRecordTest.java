package org.enlighten.challenge.mission;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TelemetryRecordTest
{
    @Test
    public void testBuilder()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .rawValue(10.11)
                .component("Component");
        TelemetryRecord record = builder.build();
        assertThat(record.getTimestamp(), is(equalTo(1L)));
        assertThat(record.getRedHighLimit(), is(equalTo(2.3)));
        assertThat(record.getYellowHighLimit(), is(equalTo(4.5)));
        assertThat(record.getYellowLowLimit(), is(equalTo(6.7)));
        assertThat(record.getRedLowLimit(), is(equalTo(8.9)));
        assertThat(record.getRawValue(), is(equalTo(10.11)));
        assertThat(record.getComponent(), is(equalTo("Component")));
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingValues()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingTimestamp()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .rawValue(10.11)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingSatelliteId()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .rawValue(10.11)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingRedHighLimit()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .rawValue(10.11)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingYellowHighLimit()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .rawValue(10.11)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingYellowLowLimit()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .redLowLimit(8.9)
                .rawValue(10.11)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingRedLowLimit()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .rawValue(10.11)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingRawValue()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .component("Component");
        builder.build();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilder_missingComponent()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redHighLimit(2.3)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .rawValue(10.11);
        builder.build();
    }
    
    @Test
    public void testRedHighLimitExceeded()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .redLowLimit(8.9)
                .component("Component")
                .redHighLimit(10.10)
                .rawValue(10.11);
        TelemetryRecord record = builder.build();
        assertTrue(record.redHighLimitExceeded());
    }
    
    @Test
    public void testRedHighValueExceeded_rawDoesNotExceed()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .component("Component")
                .redHighLimit(10.10)
                .redLowLimit(8.9)
                .rawValue(9.0);
        TelemetryRecord record = builder.build();
        assertFalse(record.redHighLimitExceeded());
    }
    
    @Test
    public void testRedLowLimitExceeded()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .component("Component")
                .redHighLimit(10.10)
                .redLowLimit(8.9)
                .rawValue(8.8);
        TelemetryRecord record = builder.build();
        assertTrue(record.redLowLimitExceeded());
    }
    
    @Test
    public void testRedLowValueExceeded_rawDoesNotExceed()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .component("Component")
                .redHighLimit(10.10)
                .redLowLimit(8.9)
                .rawValue(9.0);
        TelemetryRecord record = builder.build();
        assertFalse(record.redLowLimitExceeded());
    }
    
    @Test
    public void testYellowHighLimitExceeded()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redLowLimit(8.9)
                .component("Component")
                .redHighLimit(10.10)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .rawValue(4.6);
        TelemetryRecord record = builder.build();
        assertTrue(record.yellowHighLimitExceeded());
    }
    
    @Test
    public void testYellowHighLimitExceeded_rawDoesNotExceed()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redLowLimit(8.9)
                .component("Component")
                .redHighLimit(10.10)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .rawValue(4.0);
        TelemetryRecord record = builder.build();
        assertFalse(record.yellowHighLimitExceeded());
    }
    
    @Test
    public void testYellowLowLimitExceeded()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redLowLimit(8.9)
                .component("Component")
                .redHighLimit(10.10)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .rawValue(6.6);
        TelemetryRecord record = builder.build();
        assertTrue(record.yellowLowLimitExceeded());
    }
    
    @Test
    public void testYellowLowLimitExceeded_rawDoesNotExceed()
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1L)
                .satelliteId("SatelliteId")
                .redLowLimit(8.9)
                .component("Component")
                .redHighLimit(10.10)
                .yellowHighLimit(4.5)
                .yellowLowLimit(6.7)
                .rawValue(7.0);
        TelemetryRecord record = builder.build();
        assertFalse(record.yellowLowLimitExceeded());
    }
}
