package org.enlighten.challenge.mission;

import org.junit.Test;

import java.io.StringReader;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

public class TelemetryRecordReaderTest
{
    @Test
    public void testReadRecords() throws Exception
    {
        String telemetryFileContents = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT" + "\n"
                                     + "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT" + "\n"
                                     + "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT" + "\n";
        
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1514865665001L).satelliteId("1001").redHighLimit(101).yellowHighLimit(98).yellowLowLimit(25).redLowLimit(20).rawValue(99.9).component("TSTAT");
        TelemetryRecord expectedFirst = builder.build();
        builder.timestamp(1514865669521L).satelliteId("1000").redHighLimit(17).yellowHighLimit(15).yellowLowLimit(9).redLowLimit(8).rawValue(7.8).component("BATT");
        TelemetryRecord expectedSecond = builder.build();
        builder.timestamp(1514865686011L).satelliteId("1001").redHighLimit(101).yellowHighLimit(98).yellowLowLimit(25).redLowLimit(20).rawValue(99.8).component("TSTAT");
        TelemetryRecord expectedThird = builder.build();
        
    
        StringReader telemetry = new StringReader(telemetryFileContents);
        
        TelemetryRecordReader in = new TelemetryRecordReader(telemetry);
        TelemetryRecord first = in.next();
        assertThat(first, is(equalTo(expectedFirst)));
        
        TelemetryRecord second = in.next();
        assertThat(second, is(equalTo(expectedSecond)));
        
        TelemetryRecord third = in.next();
        assertThat(third, is(equalTo(expectedThird)));
        
        assertNull(in.next());
    }
}
