package org.enlighten.challenge.mission;

import org.junit.Test;

import static org.enlighten.challenge.mission.TelemetryRecordParser.parse;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class TelemetryRecordParserTest
{
    @Test
    public void testParseValidRecord()
    {
        //Telemetry Record Format:  <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
        String TELEMETRY_RECORD = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        builder.timestamp(1514865665001L)
                .satelliteId("1001")
                .redHighLimit(101)
                .yellowHighLimit(98)
                .yellowLowLimit(25)
                .redLowLimit(20)
                .rawValue(99.9)
                .component("TSTAT");
        TelemetryRecord expected = builder.build();
        assertThat(parse(TELEMETRY_RECORD), is(equalTo(expected)));
    }
}
