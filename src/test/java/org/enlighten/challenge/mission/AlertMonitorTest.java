package org.enlighten.challenge.mission;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AlertMonitorTest
{
    private static final SimpleDateFormat TEST_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
    
    @Test
    public void testAlertMonitor() throws Exception
    {
        AlertMonitor monitor = new AlertMonitor("TSTAT", "RED HIGH", 3, 5*60*1000, TelemetryRecord::redHighLimitExceeded);
        assertFalse (monitor.logTelemetry(buildTestRecord("20:42:12", 10, 11)));
        assertFalse (monitor.logTelemetry(buildTestRecord("20:43:12", 10, 11)));
        assertTrue (monitor.logTelemetry(buildTestRecord("20:44:12", 10, 11)));
        assertTrue (monitor.logTelemetry(buildTestRecord("20:47:13", 10, 11)));
        assertFalse (monitor.logTelemetry(buildTestRecord("20:54:12", 10, 11)));
    }
    
    @Test
    public void testGetTriggeredTelemetryRecords_onlyTriggeredTelemetryStored() throws Exception
    {
        List<TelemetryRecord> expected = new LinkedList<>();
        expected.add (buildTestRecord("20:42:12", 10, 11));
        expected.add (buildTestRecord("20:43:12", 10, 11));
        expected.add (buildTestRecord("20:47:11", 10, 11));
    
    
        AlertMonitor monitor = new AlertMonitor("TSTAT", "RED HIGH", 3, 5*60*1000, TelemetryRecord::redHighLimitExceeded);
        monitor.logTelemetry(buildTestRecord("20:42:12", 10, 11));
        monitor.logTelemetry(buildTestRecord("20:43:12", 10, 11));
        monitor.logTelemetry(buildTestRecord("20:44:12", 10, 8));
        monitor.logTelemetry(buildTestRecord("20:47:11", 10, 11));
        
        assertThat(monitor.getTriggeredTelemetryRecords(), is(equalTo(expected)));
    }
    
    
    @Test
    public void testGetTriggeredTelemetryRecords_oldRecordsAgedOff() throws Exception
    {
        List<TelemetryRecord> expected = new LinkedList<>();
        expected.add(buildTestRecord("20:54:12", 10, 11));
    
    
        AlertMonitor monitor = new AlertMonitor("TSTAT", "RED HIGH", 3, 5*60*1000, TelemetryRecord::redHighLimitExceeded);
        monitor.logTelemetry(buildTestRecord("20:42:12", 10, 11));
        monitor.logTelemetry(buildTestRecord("20:43:12", 10, 11));
        monitor.logTelemetry(buildTestRecord("20:44:12", 10, 8));
        monitor.logTelemetry(buildTestRecord("20:47:11", 10, 11));
        monitor.logTelemetry(buildTestRecord("20:54:12", 10, 11));
        
        assertThat(monitor.getTriggeredTelemetryRecords(), is(equalTo(expected)));
    }
    
    public static TelemetryRecord buildTestRecord (String date, long redHighValue, long rawValue) throws ParseException
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        long timestamp = TEST_DATE_FORMAT.parse(date).getTime();
        return builder.timestamp(timestamp).redHighLimit(redHighValue).rawValue(rawValue)
               .satelliteId("100").component("TSTAT").yellowHighLimit(0).yellowLowLimit(0).redLowLimit(0).build();
    }
}
