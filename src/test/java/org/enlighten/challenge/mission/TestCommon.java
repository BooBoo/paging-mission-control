package org.enlighten.challenge.mission;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestCommon
{
    public static final SimpleDateFormat TEST_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
    
    public static long timestamp(String date) throws ParseException
    {
        return TEST_DATE_FORMAT.parse(date).getTime();
    }
    
    /**
     * Creates a test TSTAT RED_HIGH record
     * @param date in the format:  HH:mm:ss
     * @return
     * @throws ParseException
     */
    public static TelemetryRecord createRedHighTelemetryRecord(String date) throws ParseException
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        return builder.timestamp(timestamp(date)).redHighLimit(100).rawValue(101)
                .yellowHighLimit(0).yellowLowLimit(0).redLowLimit(0).component("TSTAT").satelliteId("1000").build();
    }
    
    /**
     * Creates a test BATT RED_LOW record
     * @param date in the format:  HH:mm:ss
     * @return
     * @throws ParseException
     */
    public static TelemetryRecord createRedLowTelemetryRecord(String date) throws ParseException
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        return builder.timestamp(timestamp(date)).redLowLimit(10).rawValue(5)
                .yellowHighLimit(0).yellowLowLimit(0).redHighLimit(0).component("BATT").satelliteId("1000").build();
    }
    
    /**
     * Creates a neutral Telemetry record that won't trigger an alert
     * @param date in format:  HH:mm:ss
     * @return
     * @throws ParseException
     */
    public static TelemetryRecord createNeutralTelemetryRecord(String date) throws ParseException
    {
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        return builder.timestamp(timestamp(date)).redLowLimit(10).rawValue(20)
                .yellowHighLimit(0).yellowLowLimit(0).redHighLimit(1000).component("BATT").satelliteId("1000").build();
    }
}
