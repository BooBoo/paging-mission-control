package org.enlighten.challenge.mission;

import java.io.Closeable;

/**
 * Common static methods/definitions used by the package
 */
public class Common
{
    public static final long MILLISECONDS = 1;
    public static final long SECONDS = 1000*MILLISECONDS;
    public static final long MINUTES = 60*SECONDS;
    
    public static boolean eof(Object record)
    {
        boolean trueIfEof = (null == record);
        return trueIfEof;
    }
    
    /**
     * Closes a Closeable object, checking for null and swallowing the Exception.
     * Basically cleans a lot of boilerplate out of the code.
     * @param thingToClose
     */
    public static void close(Closeable thingToClose)
    {
        if (null == thingToClose) { return; }
        try
        {
            thingToClose.close();
        }
        catch (Exception ignored) {}
    }
    
    /**
     *
     * @param string
     * @return true if the string is null or contains nothing but whitespace.  false otherwise.
     */
    public static boolean empty(String string)
    {
        if (null == string) { return true; }
        if (string.trim().length() == 0) { return true; }
        return false;
    }
}
