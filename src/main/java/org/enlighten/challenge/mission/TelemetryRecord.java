package org.enlighten.challenge.mission;

import java.util.Objects;

/**
 * A Telemetry Record from the Satellite.  Due to the large number of values, a Builder
 * Pattern is used to clarify the values being set.
 */
public class TelemetryRecord
{
    public static class Builder
    {
        Long timestamp;
        String satelliteId;
        Double redHighLimit;
        Double yellowHighLimit;
        Double yellowLowLimit;
        Double redLowLimit;
        Double rawValue;
        String component;
        
        public Builder timestamp(long millisecondsFromEpoch)
        {
            this.timestamp = millisecondsFromEpoch;
            return this;
        }
        
        public Builder satelliteId(String satelliteId)
        {
            this.satelliteId = satelliteId;
            return this;
        }
    
        public Builder redHighLimit (double limit)
        {
            this.redHighLimit = limit;
            return this;
        }
        
        public Builder yellowHighLimit (double yellowHighLimit)
        {
            this.yellowHighLimit = yellowHighLimit;
            return this;
        }
        
        public Builder yellowLowLimit (double yellowLowLimit)
        {
            this.yellowLowLimit = yellowLowLimit;
            return this;
        }
        
        public Builder redLowLimit (double redLowLimit)
        {
            this.redLowLimit = redLowLimit;
            return this;
        }
        
        public Builder rawValue (double rawValue)
        {
            this.rawValue = rawValue;
            return this;
        }
        
        public Builder component (String component)
        {
            this.component = component;
            return this;
        }
        
        private void validateSelf()
        {
            boolean properlyFormatted = (null != timestamp) && (null != satelliteId) && (null != redHighLimit) && (null != yellowHighLimit)
                    && (null != yellowLowLimit) && (null != redLowLimit) && (null != rawValue) && (null != component);
            if (properlyFormatted) { return; }
            
            
            StringBuilder errorMsg = new StringBuilder("Invalid Record:  The following values were not set:  ");
            if (null == timestamp) { errorMsg.append("timestamp, "); }
            if (null == satelliteId) { errorMsg.append("satelliteId, "); }
            if (null == redHighLimit) { errorMsg.append("redHighLimit, "); }
            if (null == yellowHighLimit) { errorMsg.append("yellowHighLimit, "); }
            if (null == yellowLowLimit) { errorMsg.append("yellowLowLimit, "); }
            if (null == redLowLimit) { errorMsg.append("redLowLimit, "); }
            if (null == rawValue) { errorMsg.append("rawValue, "); }
            if (null == component) {errorMsg.append("component, "); }
    
            throw new IllegalStateException(errorMsg.substring(0, errorMsg.length() - 2));
        }
        
        public TelemetryRecord build()
        {
            validateSelf();
            return new TelemetryRecord(this);
        }
    }
    
    private final long timestamp;
    private final String satelliteId;
    private final double redHighLimit;
    private final double yellowHighLimit;
    private final double yellowLowLimit;
    private final double redLowLimit;
    private final double rawValue;
    private final String component;
    
    private TelemetryRecord(Builder builder)
    {
        this.timestamp = builder.timestamp;
        this.satelliteId = builder.satelliteId;
        this.redHighLimit = builder.redHighLimit;
        this.yellowHighLimit = builder.yellowHighLimit;
        this.yellowLowLimit = builder.yellowLowLimit;
        this.redLowLimit = builder.redLowLimit;
        this.rawValue = builder.rawValue;
        this.component = builder.component;
    }
    
    public boolean redHighLimitExceeded()
    {
        return rawValue > redHighLimit;
    }
    
    public boolean redLowLimitExceeded()
    {
        return rawValue < redLowLimit;
    }
    
    public boolean yellowHighLimitExceeded()
    {
        return rawValue > yellowHighLimit;
    }
    
    public boolean yellowLowLimitExceeded()
    {
        return rawValue < yellowLowLimit;
    }
    
    public long getTimestamp()
    {
        return timestamp;
    }
    
    public String getSatelliteId()
    {
        return satelliteId;
    }
    
    public double getRedHighLimit()
    {
        return redHighLimit;
    }
    
    public double getYellowHighLimit()
    {
        return yellowHighLimit;
    }
    
    public double getYellowLowLimit()
    {
        return yellowLowLimit;
    }
    
    public double getRedLowLimit()
    {
        return redLowLimit;
    }
    
    public double getRawValue()
    {
        return rawValue;
    }
    
    public String getComponent()
    {
        return component;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        TelemetryRecord that = (TelemetryRecord) o;
        return timestamp == that.timestamp &&
                Double.compare(that.redHighLimit, redHighLimit) == 0 &&
                Double.compare(that.yellowHighLimit, yellowHighLimit) == 0 &&
                Double.compare(that.yellowLowLimit, yellowLowLimit) == 0 &&
                Double.compare(that.redLowLimit, redLowLimit) == 0 &&
                Double.compare(that.rawValue, rawValue) == 0 &&
                satelliteId.equals(that.satelliteId) &&
                component.equals(that.component);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component);
    }
    
    @Override
    public String toString()
    {
        return "TelemetryRecord{" +
                "timestamp=" + timestamp +
                ", satelliteId='" + satelliteId + '\'' +
                ", redHighLimit=" + redHighLimit +
                ", yellowHighLimit=" + yellowHighLimit +
                ", yellowLowLimit=" + yellowLowLimit +
                ", redLowLimit=" + redLowLimit +
                ", rawValue=" + rawValue +
                ", component='" + component + '\'' +
                '}';
    }
}
