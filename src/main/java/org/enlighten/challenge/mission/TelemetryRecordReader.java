package org.enlighten.challenge.mission;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.util.function.Function;

import static org.enlighten.challenge.mission.Common.eof;

/**
 * Reads and parses a stream of telemetry records.
 */
public class TelemetryRecordReader implements Closeable
{
    private  final BufferedReader in;
    private  final Function<String, TelemetryRecord> telemetryRecordParser;
    
    public TelemetryRecordReader(Reader in)
    {
        this(in, TelemetryRecordParser::parse);
    }
    
    public TelemetryRecordReader (Reader in, Function<String, TelemetryRecord> telemetryRecordParser)
    {
        if (null == in) { throw new IllegalArgumentException("in cannot be null"); }
        if (null == telemetryRecordParser) { throw new IllegalArgumentException("telemetryRecordParser cannot be null"); }
        
        this.in = new BufferedReader(in);
        this.telemetryRecordParser = telemetryRecordParser;
    }
    
    /**
     *
     * @return The next TelemetryRecord in the stream.  null on EOF.
     * @throws IOException
     */
    public TelemetryRecord next() throws IOException
    {
        String rawTelemetryRecord = in.readLine();
        if (eof(rawTelemetryRecord)) { return null; }
        
        try
        {
            return telemetryRecordParser.apply(rawTelemetryRecord);
        }
        catch (Exception e)
        {
            throw new IOException("Unable to parse telemetry record: '" + rawTelemetryRecord + "'");
        }
    }
    
    public void close() throws IOException
    {
        in.close();
    }
}
