package org.enlighten.challenge.mission;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Monitors a satellite for abnormal conditions.
 */
public class SatelliteMonitor
{
    private final String satelliteId;
    private final List<AlertMonitor> alertMonitors;
    
    /**
     *
     * @param satelliteId The satellite's identifier.  Not null.
     * @param alertMonitors A list of monitors to be applied in order whenever a
     *                      TelemetryRecord is processed.  Please note that
     *                      because each satellite maintains alerts as part
     *                      of its own state, these alerts will be cloned at
     *                      construction, and the original list may be reused as
     *                      a template for other SatelliteMonitors.
     */
    public SatelliteMonitor (String satelliteId, List<AlertMonitor> alertMonitors)
    {
        if (null == satelliteId) { throw new IllegalArgumentException("satelliteId cannot be null."); }
        if (null == alertMonitors) { throw new IllegalArgumentException("alertMonitors cannot be null"); }
        
        this.satelliteId = satelliteId;
                                  // Because each Alert Monitor maintains its
                                  // own state, clone them for each Satellite
                                  // monitor (i.e. use the original as a
                                  // template).
        this.alertMonitors = cloneMonitors(alertMonitors);
    }
    
    /**
     * Applies the telemetry to each of the satellite's AlertMonitors in order.
     * @param telemetry Telemetry from a satellite
     * @return A list of any alerts generated by the given telemetry.
     */
    public List<SatelliteAlert> logTelemetry (TelemetryRecord telemetry)
    {
        List<SatelliteAlert> alerts = new LinkedList<>();
        for (AlertMonitor monitor : alertMonitors)
        {
            boolean alertTriggered = monitor.logTelemetry(telemetry);
            if (!alertTriggered) { continue; }

            long timestamp = getTimestampOfFirstAlert(monitor.getTriggeredTelemetryRecords());
            SatelliteAlert alert = new SatelliteAlert(Integer.parseInt(satelliteId), monitor.getSeverity(), monitor.getComponent(), timestamp);
            alerts.add(alert);
        }
        return alerts;
    }
    
    private static List<AlertMonitor> cloneMonitors(List<AlertMonitor> sourceMonitors)
    {
        List<AlertMonitor> clonedMonitors = new ArrayList<>(sourceMonitors.size());
        for (AlertMonitor sourceMonitor : sourceMonitors)
        {
            clonedMonitors.add(new AlertMonitor(sourceMonitor));
        }
        return clonedMonitors;
    }
    
    private static long getTimestampOfFirstAlert(List<TelemetryRecord> telemetryAlertRecords)
    {
        if (null == telemetryAlertRecords) { throw new IllegalArgumentException("telemetryAlertRecords cannot be null."); }
        if (telemetryAlertRecords.isEmpty()) { throw new IllegalStateException("telemetryAlertRecords must have at least one record."); }
        return telemetryAlertRecords.get(0).getTimestamp();
    }
}
