package org.enlighten.challenge.mission;

import java.io.IOException;
import java.util.*;

import static org.enlighten.challenge.mission.Common.eof;

/**
 * A collection of Satellites, determined by incoming telemetry, and associated monitors.
 */
public class TelemetryMonitor
{
    private final List<AlertMonitor> alertMonitors;
    
    /**
     * A collection of alerts to be applied to a stream of telemetry coming
     * from a satellite constellation.
     * @param alertMonitors A list of monitors for the satellite that are used to determine whether or not to report
     *                      abnormal satellite conditions.
     */
    public TelemetryMonitor(List<AlertMonitor> alertMonitors)
    {
        if (null == alertMonitors) { throw new IllegalArgumentException("alertMonitors cannot be null"); }
        this.alertMonitors = new ArrayList<>(alertMonitors);
    }
    
    /**
     * Analyzes a stream of telemetry from a constellation of satellites by applying a List of
     * AlertMonitors.  Alerts are gathered until until eof is reached, at which point a List of
     * those alerts will be returned.
     *
     * @param in A stream of Telemetry.  The stream ends when a null is received.
     * @return A list of Satellite Alerts that were triggered by the AlertMonitors.
     * @throws IOException
     */
    public List<SatelliteAlert> analyzeTelemetry(TelemetryRecordReader in) throws IOException
    {
        if (null == in) { throw new IllegalArgumentException("in cannot be null"); }
        
        List<SatelliteAlert> allSatelliteAlerts = new LinkedList<>();
        Map<String, SatelliteMonitor> satellites = new HashMap<>();
        TelemetryRecord telemetry = in.next();
        while (!eof(telemetry))
        {
            SatelliteMonitor satelliteMonitor = satellites.get(telemetry.getSatelliteId());
            if (null == satelliteMonitor)
            {
                satelliteMonitor = new SatelliteMonitor(telemetry.getSatelliteId(), alertMonitors);
                satellites.put(telemetry.getSatelliteId(), satelliteMonitor);
            }
            allSatelliteAlerts.addAll(satelliteMonitor.logTelemetry(telemetry));
            telemetry = in.next();
        }
        return allSatelliteAlerts;
    }
    
}