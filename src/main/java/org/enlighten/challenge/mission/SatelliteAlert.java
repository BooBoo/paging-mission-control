package org.enlighten.challenge.mission;

import java.util.Date;
import java.util.Objects;

/**
 * An alert identifying a problem with a satellite, the severity, component,
 * and timestamp of the first time the problem was noticed.
 */
public class SatelliteAlert
{
    private final int satelliteId;
    private final String severity;
    private final String component;
    private final Date timestamp;
    
    public SatelliteAlert(int satelliteId, String severity, String component, long timestampMsecEpoch)
    {
        this(satelliteId, severity, component, new Date(timestampMsecEpoch));
    }
    
    public SatelliteAlert(int satelliteId, String severity, String component, Date timestamp)
    {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = new Date(timestamp.getTime());
    }
    
    public int getSatelliteId()
    {
        return satelliteId;
    }
    
    public String getSeverity()
    {
        return severity;
    }
    
    public String getComponent()
    {
        return component;
    }
    
    public Date getTimestamp()
    {
        return new Date(timestamp.getTime());
    }
    
    @Override
    public String toString()
    {
        return "SatelliteAlert{" +
                "satelliteId=" + satelliteId +
                ", severity='" + severity + '\'' +
                ", component='" + component + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
    
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        SatelliteAlert that = (SatelliteAlert) o;
        return satelliteId == that.satelliteId &&
                Objects.equals(severity, that.severity) &&
                Objects.equals(component, that.component) &&
                Objects.equals(timestamp, that.timestamp);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }
}
