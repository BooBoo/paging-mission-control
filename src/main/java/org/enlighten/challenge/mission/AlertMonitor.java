package org.enlighten.challenge.mission;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

/**
 * A monitor for a satellite component.
 */
public class AlertMonitor
{
    private final String component;
    private final String severity;
    private final int numberOfAlerts;
    private final long samplePeriod;
    Function<TelemetryRecord, Boolean> alertTrigger;
    private final List<TelemetryRecord> triggeredTelemetryRecords = new LinkedList<>();
    
    /**
     *
     * @param component The name of the satellite component.
     * @param severity The severity level to report
     * @param numberOfAlerts The number of alerts received within the sample period needed to trigger an alert.
     * @param samplePeriodMsec The sample period in milliseconds before alerts are aged off.
     * @param alertTrigger A boolean function that determines whether or not a the TelemetryRecord has exceeded its threshold, potentially leading to an alert being thrown.
     */
    public AlertMonitor(String component, String severity, int numberOfAlerts, long samplePeriodMsec, Function<TelemetryRecord, Boolean> alertTrigger)
    {
        if (null == component) { throw new IllegalArgumentException("component cannot be null"); }
        if (null == severity) { throw new IllegalArgumentException("severity cannot be null"); }
        if (numberOfAlerts < 1) { throw new IllegalArgumentException("numberOfAlerts must be at least 1"); }
        if (samplePeriodMsec < 0) {throw new IllegalArgumentException("samplePeriodMsec must be > 0"); }
        if (null == alertTrigger) { throw new IllegalArgumentException("alertTrigger cannot be null"); }
        
        this.component = component;
        this.severity = severity;
        this.numberOfAlerts = numberOfAlerts;
        this.samplePeriod = samplePeriodMsec;
        this.alertTrigger = alertTrigger;
    }
    
    /**
     * Cloning c'tor
     * @param source The AlertMonitor to be cloned
     */
    public AlertMonitor(AlertMonitor source)
    {
        if (null == source) { throw new IllegalArgumentException("souce cannot be null."); }
        this.component = source.component;
        this.severity = source.severity;
        this.numberOfAlerts = source.numberOfAlerts;
        this.samplePeriod = source.samplePeriod;
        this.alertTrigger = source.alertTrigger;
    }
    
    /**
     * Log a telemetry record.
     * @param telemetry The telemetry record received from the satellite.
     * @return true if the alert threshold has been met within the alloted sample period signifying that an alert
     * should be reported.  False otherwise.
     */
    public boolean logTelemetry(TelemetryRecord telemetry)
    {
        if (null == telemetry) { throw new IllegalArgumentException("telemetry cannot be null"); }
        
        if (!alertTrigger.apply(telemetry)) {return false; }
    
        triggeredTelemetryRecords.add(telemetry);
        removeRecordsOutsideSamplePeriod(telemetry.getTimestamp());
        boolean trueIfAlertThresholdMet = triggeredTelemetryRecords.size() >= numberOfAlerts;
        return trueIfAlertThresholdMet;
    }
    
    public String getComponent()
    {
        return component;
    }
    
    public String getSeverity()
    {
        return severity;
    }
    
    /**
     *
     * @return A list of all the telemetry records that have triggered alerts.  Please note that this only contains
     * Telemetry records that have triggered within the AlertMonitor's sample period.
     */
    public List<TelemetryRecord> getTriggeredTelemetryRecords()
    {
        return Collections.unmodifiableList(triggeredTelemetryRecords);
    }
    
    private void removeRecordsOutsideSamplePeriod(long timestampOfCurrentRecord)
    {
        long recordAgeoffThreshold = timestampOfCurrentRecord - samplePeriod;
        triggeredTelemetryRecords.removeIf(telemetry -> telemetry.getTimestamp() < recordAgeoffThreshold);
    }
}
