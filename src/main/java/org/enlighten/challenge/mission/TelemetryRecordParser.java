package org.enlighten.challenge.mission;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Parses a telemetry record in the form:
 * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
 * into a TelemetryRecord.
 */
public class TelemetryRecordParser
{
    public static final SimpleDateFormat TELEMETRY_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
    public static TelemetryRecord parse(String telemetryRecordAsString)
    {
        if (null == telemetryRecordAsString) { return null; }
        String[] telemetryRecordParts = telemetryRecordAsString.split("\\|");
        TelemetryRecord.Builder builder = new TelemetryRecord.Builder();
        Date timestamp;
        try
        {
            timestamp = TELEMETRY_DATE_FORMAT.parse(telemetryRecordParts[0]);
        }
        catch (ParseException e)
        {
            throw new IllegalStateException("Timestamp is malformed.  Received: '" + telemetryRecordParts[0] + "'", e);
        }
        
        builder.timestamp(timestamp.getTime())
                .satelliteId(telemetryRecordParts[1])
                .redHighLimit(Double.parseDouble(telemetryRecordParts[2]))
                .yellowHighLimit(Double.parseDouble(telemetryRecordParts[3]))
                .yellowLowLimit(Double.parseDouble(telemetryRecordParts[4]))
                .redLowLimit(Double.parseDouble(telemetryRecordParts[5]))
                .rawValue(Double.parseDouble(telemetryRecordParts[6]))
                .component(telemetryRecordParts[7]);
        return builder.build();
    }
}
