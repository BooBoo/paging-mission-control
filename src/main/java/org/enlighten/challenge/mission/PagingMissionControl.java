package org.enlighten.challenge.mission;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.enlighten.challenge.mission.Common.MINUTES;
import static org.enlighten.challenge.mission.Common.close;

/**
 * main driver for the project.  See README.md for details
 */
public class PagingMissionControl
{
    public static final String TSTAT_COMPONENT = "TSTAT";
    public static final String BATTT_COMPONENT = "BATT";
    public static final String RED_HIGH_SEVERITY = "RED HIGH";
    public static final String RED_LOW_SEVERITY = "RED LOW";
    public static final int DEFAULT_NUM_ALERTS = 3;
    public static final long DEFAULT_SAMPLE_PERIOD = 5*MINUTES;
    public static final String REPORT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    public static void main (String[] args)
    {
        if (1 != args.length)
        {
            System.err.println("Usage:  java -jar pagingMissionControl.jar <telemetry filename>");
            System.exit(-1);
        }
        String filename = args[0];
        
        List<AlertMonitor> alertMonitors = new ArrayList<>(2);
        alertMonitors.add(new AlertMonitor(TSTAT_COMPONENT, RED_HIGH_SEVERITY, DEFAULT_NUM_ALERTS, DEFAULT_SAMPLE_PERIOD, telemetryRecord -> telemetryRecord.redHighLimitExceeded() && TSTAT_COMPONENT.equals(telemetryRecord.getComponent())));
        alertMonitors.add(new AlertMonitor(BATTT_COMPONENT, RED_LOW_SEVERITY, DEFAULT_NUM_ALERTS, DEFAULT_SAMPLE_PERIOD, telemetryRecord -> telemetryRecord.redLowLimitExceeded() && BATTT_COMPONENT.equals(telemetryRecord.getComponent())));
        TelemetryMonitor telemetryMonitor = new TelemetryMonitor(alertMonitors);
        
        FileReader in = null;
        List<SatelliteAlert> alerts = Collections.emptyList();
        try
        {
            in = new FileReader(filename);
            alerts = telemetryMonitor.analyzeTelemetry(new TelemetryRecordReader(in));
        }
        catch (FileNotFoundException e)
        {
            System.err.println("Error:  File: '" + filename + "' not found.  Exiting.");
            System.exit(-1);
        }
        catch (IOException e)
        {
            System.err.println("Error:  IOException while processing file: '" + filename + "'");
            System.err.println("   Exception: " + e);
            e.printStackTrace();
            System.exit(-1);
        }
        finally
        {
            close(in);
        }
        report(System.out, alerts);
    }
    
    public static void report(PrintStream out, List<SatelliteAlert> alerts)
    {
        GsonBuilder builder = new GsonBuilder().setDateFormat(REPORT_DATE_FORMAT).setPrettyPrinting();
        Gson gson = builder.create();
        out.println(gson.toJson(alerts));
    }
}
